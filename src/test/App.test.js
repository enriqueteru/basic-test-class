import { render, screen } from "@testing-library/react";
import App from '../App'
import { Logo } from "../components";

describe('[Test]- App Component', () => {
  
    test('should render image with alt logo', () => {
        render(<>
        
        <App />
        <Logo name={'kike'} />
        </>
            
            
            
            );
    
        const img = screen.getByAltText('logo');
        expect(img).toBeInTheDocument();
        expect(img).toBeVisible();
    
      });
});