import { SayMyName } from "../../components";

import { render, screen } from "@testing-library/react";

describe("[Test]-Say my name Component", () => {
  test("should render SayMyName.jsx", () => {
    //Var
    const name = "kike";
    const sum = (a, b) => a + b;

    //Render Component
    render(<SayMyName />);

    expect(name).toBe("kike");
    expect(screen.getByText(name, { exact: false })).toBeDefined();
    expect(screen.getByText(name, { exact: false })).toBeInTheDocument();
    expect(sum(2, 3)).toEqual(5);
  });
});
