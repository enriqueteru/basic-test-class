import { Logo } from "../../components";
import { render, screen } from "@testing-library/react";

describe("[Test] - Logo component", () => {
  test("Render Logo Component", () => {
    const { container } = render(<Logo />);
    expect(container).toBeInTheDocument();
  });

  test("should Render Prop Name", () => {
    const name = "kike";
    render(<Logo name={name} />);

    const selector = screen.getByText("kike", { exact: false });
    expect(selector).toBeInTheDocument();
  });

test('should  Render Subtitle', () => {
    const name = "kike";
    render(<Logo name={name} />)

    const selector = screen.getByTestId('subtitleTest');
    expect(selector).toHaveTextContent('Subtitle');
    expect(selector).toBeDefined();
});

});
